README.txt
==========

INTRODUCTION
------------

This module provides a function that acts identical to "drupal_add_js()" but
will be stored in a session instead and be executed on the next page request.

This could be useful if you want to trigger an event after a redirect etc.

set_js_queue('js/script.js', array(
  'type' => 'file',
  'scope' => 'footer',
  'weight' => 3
));

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
